package com.example.pierre.projet2emeessai;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.Allocation;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {

    Bitmap p;
    Bitmap p2;
    ImageView iv;
    private int[] save;

    /* fonction permettant de réinitialliser mon image */
    public void reset(Bitmap bmp){
        bmp.setPixels(save,0, bmp.getWidth(),0 ,0, bmp.getWidth(), bmp.getHeight());
        iv.setImageBitmap(bmp);
    }

    /* script permettant d'appliquer le filtre de niveau de gris écrit en renderscript dans grey.rs */
    private void toGreyRS(Bitmap bmp) {
        RenderScript rs = RenderScript.create(this);

        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType ());

        ScriptC_grey greyScript = new ScriptC_grey(rs);

        greyScript.forEach_toGrey(input, output);

        output.copyTo(bmp);

        input.destroy();
        output.destroy();
        greyScript.destroy();
        rs.destroy();
        iv.setImageBitmap(bmp);
    }

    /* script permettant d'appliquer le filtre pour ne garder qu'une seule couleur écrit en renderscript dans color.rs */
    private void toRedRS(Bitmap bmp) {
        RenderScript rs = RenderScript.create(this);

        Allocation input = Allocation.createFromBitmap(rs, bmp);
        Allocation output = Allocation.createTyped(rs, input.getType ());

        ScriptC_colorRed colorRedScript = new ScriptC_colorRed(rs);

        colorRedScript.forEach_toRed(input, output);

        output.copyTo(bmp);

        input.destroy();
        output.destroy();
        colorRedScript.destroy();
        rs.destroy();
        iv.setImageBitmap(bmp);
    }

    /* Le principe de cette fonction est d'appliquer un filtre pour griser l'image. Pour cela on va donc parcourir les pixels un par un à l'aide de la méthode getPixel
    et récupérer leurs trois composantes RGB afin de créer une valeur égale à  0,3*R + 0,59*G + 0,11*B. On affecte ensuite cette valeur aux trois composantes de chaque pixel
    à l'aide la fonction setPixel. */
    void toGrey(Bitmap bmp){
        for(int i=0 ; i<bmp.getWidth() ; i++){
            for(int j=0 ; j<bmp.getHeight() ; j++){
                int c = bmp.getPixel(i,j);
                int r = Color.red(c);
                int g = Color.green(c);
                int b = Color.blue(c);
                int gray = (int) (0.3 * r + 0.59 * g + 0.11 * b);
                int coding = Color.rgb(gray,gray,gray);
                bmp.setPixel(i , j , coding);
            }
        }
        iv.setImageBitmap(bmp);
    }

    /* Le principe de cette fonction est le même que celui de toGrey, seulement cette fois-ci on va utiliser les méthodes getPixels et setPixels qui permettent de manipuler
    les pixels grâce à un tableau d'entiers. */
    void toGrey2(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] tab = new int[w * h];

        bmp.getPixels(tab, 0, w, 0, 0, w, h);
        for (int i = 0; i < w*h; i++) {
            int r = Color.red(tab[i]);
            int g = Color.green(tab[i]);
            int b = Color.blue(tab[i]);
            int gray = (int) (0.3 * r + 0.59 * g + 0.11 * b);
            int coding = Color.rgb(gray, gray, gray);
            tab[i] = coding;
        }
        bmp.setPixels(tab, 0, w, 0, 0, w, h);
        iv.setImageBitmap(bmp);
    }

    /* Le principe de cette fonction est de changer la teinte de l'image. Pour se faire on parcourt les pixels de notre image et on passe ces derniers du format RGB à HSV
    qui ets un format contenant 3 nouvelles valeur dont la première qui nous intéresse et la teinte de l'image. On va alors définir cette teinte dans "newColor[o]" par une
    comprise entre 0 et 250. On repasse ensuite du format HSV au format color afin d'appliquer cette teinte à notre image. */
    void colorize(Bitmap bmp){
        float[] newColor = new float[3];
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] tab = new int[w * h];

        bmp.getPixels(tab, 0, w, 0, 0, w, h);
        for (int i = 0; i < w*h; i++) {
            int r = Color.red(tab[i]);
            int g = Color.green(tab[i]);
            int b = Color.blue(tab[i]);
            Color.RGBToHSV(r,g,b,newColor);
            newColor[0] = (int) (Math.random() * 280 );
            int coding = Color.HSVToColor(newColor);
            tab[i] = coding;
        }
        bmp.setPixels(tab, 0, w, 0, 0, w, h);
        iv.setImageBitmap(bmp);
    }

    /* Le principe de cette fonction est de ne garder qu'une seule couleur(ici le rouge). Pour cela on va manipuler les pixels et on va griser ceux pour lesquels
    le niveau de rouge est inférier (ou égal) au niveau de vert ou au niveau de bleu. Pour les autres pixels où le niveau de rouge est supérieur, on ne les modifie pas.
    Il ne restera alors que les pixels où le rouge est dominant qui seront colorés. */
    void Red(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] tab = new int[w * h];
        bmp.getPixels(tab, 0, w, 0, 0, w, h);
        for (int i = 0; i < w*h; i++) {
            int r = Color.red(tab[i]);
            int g = Color.green(tab[i]);
            int b = Color.blue(tab[i]);
            if (r<=g || r<=b){
                int gray = (int) (0.3 * r + 0.59 * g + 0.11 * b);
                int coding = Color.rgb(gray,gray,gray);
                tab[i] = coding;
            }
        }
        bmp.setPixels(tab, 0, w, 0, 0, w, h);
        iv.setImageBitmap(bmp);
    }

    /* Le principe de cette fonction est de diminuer le contraste de l'image par extension de dynamique. Pour se faire on va parcourir les pixels de notre image
    et stocker dans un tableau nommé tab le nombre de pixels ayant pour valeur l'indice de ce tableau ( soient 256 indices ). On en profite pour récupérer le pixel
    à valeur minimale de notre image et celui de valeur maximal. Ensuite, à l'aide d'un calcul que l'on éxécute dans le tableau LUT on effectue l'extension
    de dynamique sur les pixels de notre image. */
    void extension(Bitmap bmp) {
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] p = new int[w*h];
        int[] tab = new int[256];
        int[] LUT = new int[256];
        int min = 0;
        int max = 0;

        bmp.getPixels(p, 0, w, 0, 0, w, h);
        for (int i = 0; i < w*h; i++) {
            tab[Color.red(p[i])]++;
            if (Color.red(p[i]) > max)
                max = Color.red(p[i]);
        }
        while (tab[min]==0)
            min++;

        if (max == min)
            return;

        for (int ng =0; ng < 256; ng++) {
            LUT[ng] = 255 * (ng - min) / (max - min);
            LUT[ng] = Color.rgb(LUT[ng], LUT[ng], LUT[ng]);
        }

        for (int i = 0; i < w*h; i++)
            p[i] = LUT[Color.red(p[i])];

        bmp.setPixels(p, 0, w, 0, 0, w, h);
        iv.setImageBitmap(bmp);
    }

    /* Le principe de cette fonction est cette fois d'augmenter le contraste de l'image par égalisation d'histogramme. Pour se faire on va parcourir les pixels de notre image
    et les stocker dans un tableau nommé "hist" le nombre de pixels ayant pour valeur l'indice de ce tableau ( soient 256 indices ). On va ensuite calculer le tableau C qui est le cumule
    de hist, autrement la valeur a l'indice n du tableau C contient la somme des valeur de hist allant de l'indice 0 à n. On applique ensuite la formule a chacunes des nos composantes
    RGB des pixels afin d'augmenter le contraste. */
    void equalization(Bitmap bmp){
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] p = new int[w*h];
        int[] hist = new int[256];
        int[] C = new int[256];
        int[] T = new int[256];

        bmp.getPixels(p, 0, w, 0, 0, w, h);
        for (int i = 0; i < w*h; i++) {
            hist[Color.red(p[i])]++;
        }

        C[0] = hist[0];
        for (int i = 1; i < 256; i++) {
            C[i] = C[i-1] + hist[i];
        }

        for (int i = 0; i < w*h; i++) {
            int r = Color.red(p[i]);
            int g = Color.green(p[i]);
            int b = Color.blue(p[i]);
            r = (C[r]*255/(w*h));
            g = (C[g]*255)/(w*h);
            b = (C[b]*255)/(w*h);
            int coding = Color.rgb(r,g,b);
            p[i] = coding;
        }

        bmp.setPixels(p, 0, w, 0, 0, w, h);
        iv.setImageBitmap(bmp);
    }

    /* Le principe de cette fonction est d'appliquer un filtre convolution afin de "flouter" notre image. Pour se faire j'ai appliqué une convolution
     pour une taille de fenêtre 3x3. Pour se faire, je parcours les pixels de mon image (mis à part les pixels du contour de l'image). Pour chacun des pixels
     étudiés je vais ensuite additionnés les valeurs des différentes composantes RGB du pixel étudié et des 8 qui sont autour de lui, afin d'ensuite faire la
     moyenne des 9 pixels en question. */
    void convolution(Bitmap bmp){
        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int[] p = new int[w*h];
        int[] moy = new int[w*h];
        //int somme;

        bmp.getPixels(p, 0, w, 0, 0, w, h);
        for (int i = 1; i < h-1; i++){
            for (int j = 1; j < w-1; j++){
                int r = 0;
                int g = 0;
                int b = 0;
                for (int x = i - 1; x < i + 2; x++) {
                    for (int y = j - 1; y < j + 2; y++) {
                        r = r + Color.red(p[x * w + y]);
                        g = g + Color.green(p[x * w + y]);
                        b= b + Color.blue(p[x * w + y]);
                    }
                }
                r = Math.round(r / 9);
                g = Math.round(g / 9);
                b = Math.round(b / 9);
                moy[i * w + j] = Color.rgb(r, g, b);
            }
        }
        bmp.setPixels(moy, 0, w, 0, 0, w, h);
        iv.setImageBitmap(bmp);
    }

    public View.OnClickListener toGreyRSButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            toGreyRS(p2);
        }
    };

    public View.OnClickListener toRedRSButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            toRedRS(p2);
        }
    };

    public View.OnClickListener toGreyButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            toGrey(p2);
        }
    };

    public View.OnClickListener toGrey2ButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            toGrey2(p2);
        }
    };

    public View.OnClickListener colorizeButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            colorize(p2);
        }
    };

    public View.OnClickListener RedButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            Red(p2);
        }
    };

    public View.OnClickListener Reset = new View.OnClickListener(){
        public void onClick(View v){
            reset(p2);
        }
    };

    public View.OnClickListener extensionButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            extension(p2);
        }
    };

    public View.OnClickListener equalizationButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            equalization(p2);
        }
    };

    public View.OnClickListener convolutionButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            convolution(p2);
        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BitmapFactory.Options b = new BitmapFactory.Options();
        b.inMutable = true;
        b.inScaled = false;
        p = BitmapFactory.decodeResource(getResources(),R.drawable.image,b);
        save = new int[p.getWidth() * p.getHeight()];
        p.getPixels(save, 0, p.getWidth(), 0, 0, p.getWidth(), p.getHeight());

        p2 = p.copy(p.getConfig(), true);

        iv = (ImageView) findViewById(R.id.im);
        iv.setImageBitmap(p2);

        TextView tv=(TextView) findViewById(R.id.text);
        tv.setText("Width = " + p.getWidth() + " Height = " + p.getHeight());

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(toGreyRSButtonListener);

        Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(toGreyButtonListener);

        Button button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(toGrey2ButtonListener);

        Button button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(colorizeButtonListener);

        Button button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(RedButtonListener);

        Button button6 = (Button) findViewById(R.id.button6);
        button6.setOnClickListener(Reset);

        Button button7 = (Button) findViewById(R.id.button7);
        button7.setOnClickListener(extensionButtonListener);

        Button button8 = (Button) findViewById(R.id.button8);
        button8.setOnClickListener(equalizationButtonListener);

        Button button9 = (Button) findViewById(R.id.button9);
        button9.setOnClickListener(convolutionButtonListener);

        Button button10 = (Button) findViewById(R.id.button10);
        button10.setOnClickListener(toRedRSButtonListener);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("CV", "on start");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.i("CV","on resume");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.i("CV","on pause");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.i("CV","on stop");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("CV","on restart");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("CV","on destroy");
    }

}
