#pragma  version (1)
#pragma  rs java_package_name(com.example.pierre.projet2emeessai);

uchar4  RS_KERNEL  invert(uchar4 in, uint32_t x, uint32_t y) {
    uchar4  out = in;
    out.r = 255 - in.r;
    out.g = 255 - in.g;
    out.b = 255 - in.b;
    return  out;
}

uchar4  RS_KERNEL  toGrey(uchar4  in) {
    float4  pixelf = rsUnpackColor8888(in);
    float  grey = (0.30* pixelf.r + 0.59* pixelf.g + 0.11* pixelf.b);
    return  rsPackColorTo8888(grey , grey , grey , pixelf.a);
}